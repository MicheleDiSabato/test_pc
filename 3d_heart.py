from time import time
print('Importing libraries...')
start = time()
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import random
seed = 427
random.seed(seed)
os.environ['PYTHONHASHSEED'] = str(seed)
np.random.seed(seed)
end = time()
print(f'...libraries imported in {np.ceil(end-start)} seconds')

print('Loading data...')
start = time()
df_og = pd.read_csv(os.getcwd() + os.sep + 'dataset_test_cpu' + os.sep + 'LAsignals.csv')
df_og = df_og.iloc[:,-len(df_og.columns.values)+1:]
start_index = 6 # index used to select only the signals from df
signals = df_og.iloc[:,start_index:].values
coordinates = df_og.iloc[:,:3]
coordinates = coordinates.values.astype('<f4')
end = time()
print(f'...data loaded in {np.ceil(end-start)} seconds')

print('Start plot...')
start = time()
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter3D(coordinates[::10,0], coordinates[::10,1], coordinates[::10,2])
plt.show()
end = time()
print(f'...plot ended in {(end-start)} seconds')