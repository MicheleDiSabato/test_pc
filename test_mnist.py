from time import time
start = time()
print('Importing libraries...')
from tensorflow import keras as tfk
from tensorflow.keras import layers as tfkl
import tensorflow as tf
import numpy as np
import os 
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
end = time()
print(f'...libraries imported in {(end-start)} seconds')

start = time()
print('Loading MNIST dataset...')
(x_train, y_train), (x_test, y_test) = tfk.datasets.mnist.load_data()
x_train = x_train/255
x_test = x_test/255
y_train = tfk.utils.to_categorical(y_train)
y_test = tfk.utils.to_categorical(y_test)
X_train, X_val, y_train, y_val = train_test_split(x_train, y_train, test_size=.3, stratify=y_train)
end = time()
print(f'...MNIST dataset loaded in {(end-start)} seconds')

print('Compiling model...')
start = time()
def build_model_light(input_shape = (28,28,1)):

    input_layer = tfkl.Input(shape=input_shape, name='Input')

    c = tfkl.Conv2D(filters=3, kernel_size=(3, 3), activation = 'relu')(input_layer)
    c = tfkl.Conv2D(filters=2, kernel_size=(3, 3), activation = 'relu')(c)
    c = tfkl.Conv2D(filters=1, kernel_size=(3, 3), activation = 'relu')(c)
    # mp = tfkl.MaxPooling2D(pool_size = (2, 2))(c)
    
    flattening_layer = tfkl.Flatten()(c)
    dropout1 = tfkl.Dropout(0.3)(flattening_layer)
    classifier_layer1 = tfkl.Dense(units=64, activation='relu')(dropout1)
    dropout2 = tfkl.Dropout(0.3)(classifier_layer1)
    classifier_layer2 = tfkl.Dense(units=32)(dropout2)
    output_layer = tfkl.Dense(units=10, activation='softmax')(classifier_layer2)

    model = tfk.Model(inputs=input_layer, outputs=output_layer, name='model')

    lr = 1e-3
    model.compile(loss=tfk.losses.CategoricalCrossentropy(), optimizer=tfk.optimizers.Adam(learning_rate=lr), metrics='accuracy')

    return model, 'light'

def build_model_heavy(input_shape = (28,28,1)):

    input_layer = tfkl.Input(shape=input_shape, name='Input')

    f = tfkl.Flatten()(input_layer)
    do = tfkl.Dropout(0.3)(f)
    cl = tfkl.Dense(units=512, activation='relu')(do)
    do = tfkl.Dropout(0.3)(cl)
    cl = tfkl.Dense(units=512, activation='relu')(do)
    do = tfkl.Dropout(0.3)(cl)
    cl = tfkl.Dense(units=512, activation='relu')(do)
    d0 = tfkl.Dropout(0.3)(cl)
    cl = tfkl.Dense(units=256, activation = 'relu')(do)
    d0 = tfkl.Dropout(0.3)(cl)
    cl = tfkl.Dense(units=256, activation = 'relu')(do)
    d0 = tfkl.Dropout(0.3)(cl)
    cl = tfkl.Dense(units=256, activation = 'relu')(do)
    do = tfkl.Dropout(0.3)(cl)
    cl = tfkl.Dense(units=128, activation='relu')(do)
    do = tfkl.Dropout(0.3)(cl)
    cl = tfkl.Dense(units=64, activation='relu')(do)
    do = tfkl.Dropout(0.3)(cl)
    cl = tfkl.Dense(units=32, activation='relu')(do)
    output_layer = tfkl.Dense(units=10, activation='softmax')(cl)

    model = tfk.Model(inputs=input_layer, outputs=output_layer, name='model')

    lr = 1e-3
    model.compile(loss=tfk.losses.CategoricalCrossentropy(), optimizer=tfk.optimizers.Adam(learning_rate=lr), metrics='accuracy')

    return model, 'heavy'

model, model_name = build_model_light((28,28,1))
model.summary()
end = time()
print(f'...compiling ended in {(end-start)} seconds')

print('Fitting the model....')
start = time()
epochs = 4 if model_name == 'light' else 10
history = model.fit(
    x = X_train,
    y = y_train,
    batch_size = 128,
    epochs = epochs,
    validation_data = (X_val, y_val),
    #callbacks = [tfk.callbacks.EarlyStopping(monitor='val_accuracy', mode='max', patience=10, restore_best_weights=True)]
).history
end = time()
print(f'...fitting ended in {np.ceil((end-start)/60)} minutes')

print('Plotting accuracy and loss on training and validation...')
start = time()
plt.figure(figsize=(15,5))
plt.plot(history['loss'], label='Training', alpha=.8, color='#ff7f0e')
plt.plot(history['val_loss'], label='Validation', alpha=.8, color='#4D61E2')
plt.legend(loc='upper left')
plt.title('Binary Crossentropy')
plt.grid(alpha=.3)
plt.show(block = False)

plt.figure(figsize=(15,5))
plt.plot(history['accuracy'], label='Training', alpha=.8, color='#ff7f0e')
plt.plot(history['val_accuracy'], label='Validation', alpha=.8, color='#4D61E2')
plt.legend(loc='upper left')
plt.title('Accuracy')
plt.grid(alpha=.3)
plt.show(block = False)
end = time()
print(f'...plot of accuracy and loss ended in {(end-start)} seconds')

print('Prediction on test set...')
start = time()
pis = [1, 12, 123, 1234, 5432, 6544, 1233, 543]
predictions = model.predict(x_test)
labels = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6', 7:'7', 8:'8', 9:'9'}
for prediction_index in pis:
    print(predictions[prediction_index])

    fig, (ax1, ax2) = plt.subplots(1,2)
    fig.set_size_inches(15,5)
    ax1.imshow(x_test[prediction_index])
    ax1.set_title('True label: '+labels[np.argmax(y_test[prediction_index])])
    ax2.barh(list(labels.values()), predictions[prediction_index], color=plt.get_cmap('Paired').colors)
    ax2.set_title('Predicted label: '+labels[np.argmax(predictions[prediction_index])])
    ax2.grid(alpha=.3)
    if prediction_index == pis[-1]:
        plt.show()
    else:
        plt.show(block = False)
end = time()
print(f'...prediction on test set and plots finished in {np.ceil((end-start)/60)} minutes')