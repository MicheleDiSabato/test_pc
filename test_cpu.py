from time import time
print('Importing libraries...')
start = time()
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import random
from sklearn.metrics.pairwise import euclidean_distances
seed = 427
random.seed(seed)
os.environ['PYTHONHASHSEED'] = str(seed)
np.random.seed(seed)
end = time()
print(f'...libraries imported in {np.ceil(end-start)} seconds')

print('Loading data...')
start = time()
df_og = pd.read_csv(os.getcwd() + os.sep + 'dataset_test_cpu' + os.sep + 'LAsignals.csv')
# remove index column
df_og = df_og.iloc[:,-len(df_og.columns.values)+1:]
# tidy the dataset
start_index = 6 # index used to select only the signals from df
signals = df_og.iloc[:,start_index:].values
coordinates = df_og.iloc[:,:3]
coordinates = coordinates.values.astype('<f4')
UAC = df_og.iloc[:,3:5].values
IIR = df_og.iloc[:,5].values.astype('<f2')
aligned_signals = pd.read_csv(os.getcwd() + os.sep + 'dataset_test_cpu' + os.sep + 'aligned_LAsignals.csv', header = None)
end = time()
print(f'...data loaded in {np.ceil(end-start)} seconds')

print('Defining teseo function...')
start = time()
def teseo(n, threshold, B = 500, K = 2):
  classification_matrix = np.zeros((coordinates.shape[0], 1)).astype('<i2')
  average_normalized_entropy = np.zeros((B,1))
  for b in range(B):
    t0 = time()
    nuclei_indices = np.array(random.sample(list(np.arange(coordinates.shape[0])), n)) 
    nuclei_coordinates = np.zeros((n, coordinates.shape[1])) 
    nuclei_coordinates = coordinates[nuclei_indices, :]
    distances = euclidean_distances(coordinates, nuclei_coordinates)
    min_index = np.argmin(distances, axis = 1)
    weights = np.zeros(coordinates.shape[0])
    idx = np.arange(coordinates.shape[0])
    weights[nuclei_indices] = 1
    del distances
    representative = np.zeros((n, aligned_signals.shape[1]))  
    representative = aligned_signals.values[nuclei_indices]
    del weights
    fd = np.diff(representative)
    teta = 5
    d = np.sqrt(euclidean_distances(representative, representative)**2 + teta*euclidean_distances(fd, fd)**2)
    c = np.sum(d, axis = 1)
    tau = np.quantile(c, threshold)
    labels = np.zeros_like(c, dtype = int)
    labels[c < tau] = 1
    labels = labels.reshape(-1,)
    idx = np.arange(coordinates.shape[0])
    for i in range(n):
      if labels[i] == 1:
        classification_matrix[idx[min_index == i]] += 1
    p1 = classification_matrix / (b+1)
    p0 = 1 - p1
    v0 = np.zeros((coordinates.shape[0], 1))
    v1 = np.zeros((coordinates.shape[0], 1))
    v0[p0 != 0] = p0[p0 != 0] * np.log(p0[p0 != 0])
    v1[p1 != 0] = p1[p1 != 0] * np.log(p1[p1 != 0])
    average_normalized_entropy[b] = - np.sum(( v0 + v1 )) / (np.log(2) * coordinates.shape[0])
    del p0, p1, v0, v1, labels, idx
    t1 = time()
    delta_t = (t1-t0)/60
    total_time_estimate = np.ceil(delta_t * (B-b-1))
    if b < B-1:
      print(f'{b+1} - ETA: {total_time_estimate} minutes', end = "\r")
    if b == B-1:
      print(f'{b+1} - ETA: {total_time_estimate} minutes')
  return classification_matrix
end = time()
print(f'...teseo function defined in {np.ceil(end - start)} seconds')

print('Starting loop...')
start = time()
# thresholds = [0.01, 0.05, 0.1, 0.2]
# ns = [50, 100, 200, 300, 1000]
thresholds = [0.01]
ns = [1000]
for n in ns:
    for thr in thresholds:
        print("n:", n, ";", "thr:", thr)
        classification_matrix = teseo(n, thr, B = 500, K = 2)
end = time()
print(f'...loop ended in {(end-start)/60} minutes')